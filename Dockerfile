FROM python:3.8

WORKDIR /home/ec2-user/bmi_sayur

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY . .

CMD ["python3.8", "./main.py", "-p", "8080"]
