import json
import gevent.monkey
import bottle

from argparse import ArgumentParser
from bottle import get, request, response, run


def enable_cors(fn):
    def _enable_cors(*args, **kwargs):
        # set CORS headers
        response.headers['Access-Control-Allow-Origin'] = '*'
        response.headers['Access-Control-Allow-Methods'] = 'GET, POST, PUT, OPTIONS'
        response.headers['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, X-Requested-With, ' \
                                                           'X-CSRF-Token '

        if bottle.request.method != 'OPTIONS':
            # actual request; reply with the actual response
            return fn(*args, **kwargs)

    return _enable_cors


def get_label(point):
    if point < 18.5:
        label = 'under_weight'
    elif 18.5 < point < 24.9:
        label = 'normal'
    elif point > 24.9:
        label = 'overweight'

    return label


def calculate(weight_kg, height_m):
    ops = weight_kg / (height_m ** 2)
    return ops

@get('/home')
def hello_world():
    return 'Hello, Docker!'

@get('/')
def summary():
    height_cm = float(request.GET.get('height'))
    weight_kg = float(request.GET.get('weight'))

    height_m = height_cm / 100

    num = round(calculate(weight_kg, height_m), 2)
    lab = get_label(num)

    result = {'bmi': num, 'label': lab}

    return json.dumps(result)


if __name__ == '__main__':
    app = bottle.app()
    parser = ArgumentParser(description="BMI API")
    parser.add_argument('-p', '--port', help='Port for API.', default='8000')
    args = parser.parse_args()
    port = args.port
    # http://localhost:8000/?height=180&weight=90
    gevent.monkey.patch_all(thread=True)
    run(
        host='0.0.0.0',
        port=int(port),
        debug=True,
        server='gevent',
        reloader=True
    )
